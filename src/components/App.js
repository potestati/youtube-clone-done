import React from 'react';
import SearchBar from './SearchBar';
import youtube from '../apis/youtube';
import VideoList from './VideoList';
import VideoDetail from './VideoDetail';

const KEY = 'AIzaSyCN6e98GjAfa_d4E4mISSn8uCP6g5b1DvE';

class App extends React.Component {

  //posto treba da ucitamo vidoe onda cemo definisati u state novi property videos i to ce naravno biti array

  //ocekujemo ispis liste videa sto je prazan niz i trebace nam zeljeni selektovan video sad je inicijalno null
  state = { videos: [], selectedVideo: null };

  //componentDidMount() ce nam u ovom slucaju obezbediti da ne dobijemo praznu sliku kada pokrenemo app prilikom prvog ucitavanja. Ova komponenta ce obezbediti nasumicno po nekom osnovu ispisa liste video snimaka. Ova komponenta ce napraviti neki default search i u argument cemo ubaciti neki default termin ili random

  componentDidMount() {
    //u body ove komponente cemo ubaciti deo koda od gore koji radi celu logiku upita inace pod uslovom da se ubaci termin u search polje
    this.onTermSubmit('buildings')
  }
  //term je termin koji se unosi u search bar za pretragu videa sa youtube
  onTermSubmit = async term => {
    const response = await youtube.get('/search', {
      params: {
        q: term,
        part: 'snippet',
        maxResults: 5,
        type: 'video',
        key: KEY,
      },
    });
    //provericemo sta dobijamo u responsu koji uzimamo youtube.get()

    console.log(response);

    //nama trebaju ovi podaci iz requesta ka apiju youtube

    // response.data.items;

    //posto smo definisali state {vedoes sa praznim array-om on sada ovde treba da dobije podatke , nakon response tj.dobijanja podataka}

    {/* ovde props videos dobija NOVU vrednost IZ RESPONSA*/ }

    //sada cemo sa setState da promenimo vreednost state sa poctka ovog fajla i to za parametar videos, mozda treba ovde ubaciti i za selektovan video default vrednost ukoliko jos uvek nije zatrazen ni jedan termin u search polju
    this.setState({
      videos: response.data.items,
      //kada unesemo neki termin prvi video na listi i na koji se naidje bice veliki otvoren video sa detaljima sa leve strane
      //kao test samo treba zakomentarisati ovaj red ispod
      selectedVideo: response.data.items[0]
    });
  };

  //ova funkcija treba da ima za uslov neku vrednost u objektu video pa onda da proradi

  onVideoSelect = (video) => {
    //ovde program pokupi podatak koji je video selektovan
    this.setState({ selectedVideo: video });
  };

  render() {
    return (
      // return search bar for videos 
      <div className="ui container">
        <SearchBar onFormSubmit={this.onTermSubmit} />

        {/*return details about selected video i dalje se prosledjuje kroz varijablu video u ostale fajlove video details i video items koji cemo pustiti*/}
        <div className="ui grid">
          <div className="ui row">
            <div className="eleven wide column">
              <div>
                <VideoDetail video={this.state.selectedVideo} />
              </div>
              {/* I have {this.state.videos.length} videos. */}
              {/* ovde PROPERTI videos dobija vrednost IZ GORE SETSTATE VIDEOS: NOVA VREDNOST , OVA VREDNOST SE SALJE KAO PROPS U KOMPONENTU VideoList i u taj fajl onda i to kroz property videos*/}
            </div>
            {/* return video listu svih nadjenih videa sa youtube */}
            <div className="five wide column">
              <VideoList
                onVideoSelect={this.onVideoSelect}
                //this.state.videos su ustvari sve iz state sto je ustvari response.data.items pa onda taj niz ima vise videa sto je onda niz nizova i onda svaki taj item predstavlja jedan podniz koji ovde zovemo vides
                videos={this.state.videos} />
            </div>

          </div>
        </div>
      </div>
    );
  }
}

export default App;
