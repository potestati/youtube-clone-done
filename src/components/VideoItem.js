import './VideoItem.css';
import React from 'react';

const VideoItem = ({ video, onVideoSelect }) => {
    return (

        //dole onClick poziva funkciju onVideoSelect iz App.js fajla kroz fajl VideoList.js i to kroz arrow funkciju ovde kako bi onClick bio pokupljen i selektovan video ustvari, da smo samo napisali onVideoSelect ne bi bio pokupljen video

        <div onClick={()=>onVideoSelect(video)} className="video-item item">
            <img 
            alt={video.snippet.title} 
            className="ui image" 
            src={video.snippet.thumbnails.medium.url} />
            <div className="content">
                <div className="header">{video.snippet.title}</div>
            </div>
        </div>
    )
};

export default VideoItem;

//onClick omogucava da kada se klikne na jedan video item otvori se fajl videoDetail.js i tu se ispisuje title i description