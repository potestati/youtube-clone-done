import React from 'react';

// objekat video je ustvari kada bi isli props.video
const VideoDetail = ({ video }) => {
    //if ce definisati sta ako selektovan video ne postoji tj.ako je selectedVideo = 

    if (!video) {
        return (
            <div>Loading...</div>
        );
    }
    const videoSrc = `https://www.youtube.com/embed/${video.id.videoId}`
    return (
        <div>
        {/* klasa embed u sematic ui sluzi da se video lepo uklopi */}
        <div className="ui embed">
            <iframe title="video player" src={videoSrc} />
        </div>
            <div className="ui segment">
                <h4 className="ui header">{video.snippet.title}</h4>
                <p>{video.snippet.description}</p>
            </div>
        </div>
    );
};

export default VideoDetail;
